/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web

/**
  * Enumeration containing the HTTP status codes used by TuSoW's
  * REST API.<br>
  * Please refer to <a href="https://app.swaggerhub.com/apis/lrizzato/TuSoW/1.0.0">TuSoW REST API</a>
  * for further information.
  */
object StatusCodes {
  final val OK = 200
  final val CREATED = 201
  final val ACCEPTED = 202
  final val NO_CONTENT = 204
}
