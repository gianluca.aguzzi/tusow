/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web

import io.vertx.core.json.JsonObject
import io.vertx.scala.core.DeploymentOptions
import scopt.OptionParser

case class TusowArgs(port: Int = C.defaults.port, root: String = C.defaults.root) {
  def toJsonObject: JsonObject = {
    new JsonObject()
      .put(C.keys.port, port)
      .put(C.keys.root, root)
  }

  def toDeploymentOptions: DeploymentOptions = {
    DeploymentOptions().setConfig(this.toJsonObject)
  }
}

object TusowArgs {

  // https://github.com/scopt/scopt
  private lazy val parser = new OptionParser[TusowArgs]("TuSoW") {
    head("TuSoW - Tuple Spaces over the Web")

    opt[Int]('p', "port")
      .text(s"Service port. Defaults to ${C.defaults.port}")
      .optional()
      .action((p, args) => args.copy(port = p))

    opt[String]('r', "root")
      .text(s"Service root path. Defaults to ${C.defaults.root}")
      .optional()
      .action((r, args) => args.copy(root = s"/$r"))
  }

  def apply(args: Seq[String]): Option[TusowArgs] = parser.parse(args, TusowArgs())
}