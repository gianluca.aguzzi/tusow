/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web.route.handler

import java.io.IOException

import alice.tuprolog.Term
import com.fasterxml.jackson.databind.JsonNode
import com.jayway.jsonpath.JsonPath
import io.vertx.core.Handler
import io.vertx.scala.ext.web.{Router, RoutingContext}
import it.unibo.tusow.JsonAsyncDataSpace
import it.unibo.tusow.web.C
import it.unibo.tusow.web.DataSpaceHttpProxy.CONTENT_TYPE_HEADER
import it.unibo.tusow.web.StatusCodes.{ACCEPTED, CREATED}
import it.unibo.tusow.web.controller.{DataSpaceController, JsonDataSpaceController}
import it.unibo.utils.json.Extensions._

import scala.collection.mutable
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

/**
  * Intercepts and handles HTTP requests directed towards Json data spaces.
  * <br>
  * If a request addresses a data space whose name is not known to this handler,
  * a new data space with the requested name is created.
  *
  * @param executionContext the concurrent execution context
  */
class JsonRequestHandler(implicit executionContext: ExecutionContext)
  extends DataSpaceRouteHandler[JsonNode, JsonPath] {

  override protected def spawnNewController(): DataSpaceController[JsonNode, JsonPath] =
    JsonDataSpaceController(JsonAsyncDataSpace())

  override def bindTo(router: Router, route: String): Unit = {
    super.bindTo(router, route)
    router.post(route)
      .consumes("application/yaml")
      .produces(contentType)
      .handler(handlePost())
      .handler(_.next())
  }

  override protected def contentType: String = "application/json"

  override protected def acceptsDatum: String = "application/json"

  override protected def dataSeparator: String = C.defaults.sep

  override protected def payloadToDatum(payloadDatum: String): JsonNode = payloadDatum.toJsonNode

  override protected def dataToString(optDatum: Seq[JsonNode]): String = optDatum.mkString("[", ",", "]")

  override protected def queryToTemplate(query: String): JsonPath = query.toJsonPath

  override protected def payloadToData(payloadDatum: String): Seq[JsonNode] = {
    val result: JsonNode = payloadDatum.toJsonNode

    if (result.isArray) {
      jsonArrayToSeq(result)
    } else {
      Seq(result)
    }
  }

  private def jsonArrayToSeq(xs: JsonNode): Seq[JsonNode] = {
    val result : mutable.Buffer[JsonNode] = mutable.ListBuffer[JsonNode]()
    xs.elements().forEachRemaining((t: JsonNode) => result.append(t))
    result
  }
}
