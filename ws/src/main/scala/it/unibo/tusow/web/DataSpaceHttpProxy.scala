/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web

import io.vertx.core.{AsyncResult, Handler}
import io.vertx.lang.scala.ScalaVerticle
import io.vertx.scala.core.http.HttpServer
import io.vertx.scala.ext.web.handler.{BodyHandler, LoggerHandler}
import io.vertx.scala.ext.web.{Router, RoutingContext}
import it.unibo.tusow.web.DataSpaceHttpProxy.{JSON_DS_ROUTE, L, PROLOG_DS_ROUTE, STRING_DS_ROUTE}
import it.unibo.tusow.web.route.handler.{JsonRequestHandler, PrologRequestHandler, StringRequestHandler}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success}

/**
  * This class encapsulates a Vertx HTTP server. It is capable of intercepting
  * HTTP requests and defines the default routes and parameters names
  * found in <a href="https://app.swaggerhub.com/apis/lrizzato/TuSoW/1.0.0">TuSoW REST API</a>.
  */
class DataSpaceHttpProxy extends ScalaVerticle {

  private var server: HttpServer = null
  private var router: Router = null

  /**
    * Setup the server and the request handlers.<br>
    * Every handler must be instantiated here and bound appropriately
    * with a corresponding route.
    */
  override def startFuture(): Future[_] = {
    val start = Promise[Unit]

    server = vertx.createHttpServer()
    router = Router.router(vertx)

    router
      .route()
      .handler(LoggerHandler.create())
      .handler(BodyHandler.create())

    val jsonHandler = new JsonRequestHandler()
    val stringHandler = new StringRequestHandler()
    val prologHandler = new PrologRequestHandler()

    jsonHandler.bindTo(router, path(JSON_DS_ROUTE))
    stringHandler.bindTo(router, path(STRING_DS_ROUTE))
    prologHandler.bindTo(router, path(PROLOG_DS_ROUTE))

    return server
      .requestHandler(router.accept _)
      .listenFuture(port)
      .andThen {
        case Success(x) => {
          L.info(s"Listening on http://localhost:${server.actualPort()}$root")
          Success(x)
        }
        case Failure(e) => Failure(e)
      }

  }

  override def stopFuture(): Future[_] = {
    val stop = Promise[Unit]

    server.close((_: AsyncResult[Unit]) => {
      L.info(s"Not listening anymore")
      stop.complete(Success(()))
    })

    return stop.future
  }

  /**
    * Convenience method for logging incoming requests.
    *
    * @param ctx the routing context
    */
  private def logRequest(ctx: RoutingContext): Unit = {
    val sep = "\n    "
    val method = ctx.request().rawMethod()
    val url = ctx.request().uri()
    val query = ctx.request().query()
      .map(_.replace(raw"&", sep))
      .map(_.replace("=", " = "))
      .map(sep + _).getOrElse("")
    val body = ctx.getBodyAsString().map(sep + _).getOrElse("")
    DataSpaceHttpProxy.L.info(s"$method $url$query$body")
    ctx.next()
  }

  def port: Int =
    ctx.config().flatMap(jo => Option(jo.getInteger(C.keys.port))).map(_.intValue).getOrElse(C.defaults.port)

  def root: String =
    ctx.config().flatMap(jo => Option(jo.getString(C.keys.root))).getOrElse(C.defaults.root)

  protected final def path(subPath: String): String =
    root + subPath
}

/**
  * DataSpaceHttpProxy's companion object.
  * It defines some of the default values, routes and parameter names
  * found in <a href="https://app.swaggerhub.com/apis/lrizzato/TuSoW/1.0.0">TuSoW REST API</a>.
  */
private object DataSpaceHttpProxy {

  val L: Logger = LoggerFactory.getLogger(classOf[DataSpaceHttpProxy])

  /**
    * Dataspace parameter name.
    */
  val PARAM_DS: String = "ds"
  /**
    * "Template" parameter name.
    */
  val PARAM_TEMPLATE: String = "template"
  /**
    * "Predicative" parameter name.
    */
  val PARAM_PREDICATIVE: String = "predicative"

  val PARAM_BULK: String = "bulk"

  /**
    * "Synchronous" parameter name.
    */
  val PARAM_SYNCHRONOUS: String = "sync"

  /**
    * Default affirmative value for true/false query parameters.
    */
  val AFFIRMATIVE_VALUE: String = "true"
  /**
    * Default negative value for true/false query parameters.
    */
  val NEGATIVE_VALUE: String = "false"

  /**
    * Convenience constant to store the HTTP content type header name.
    */
  val CONTENT_TYPE_HEADER: String = "Content-Type"

  /**
    * The default route for addressing JSON dataspaces.
    */
  val JSON_DS_ROUTE = s"/json/:$PARAM_DS"

  /**
    * The default route for addressing textual dataspaces.
    */
  val STRING_DS_ROUTE = s"/string/:$PARAM_DS"

  /**
    * The default route for addressing textual dataspaces.
    */
  val PROLOG_DS_ROUTE = s"/prolog/:$PARAM_DS"

  val DEFAULT_PORT: Int = C.defaults.port

  val DEFAULT_ROOT: String = C.defaults.root
}
