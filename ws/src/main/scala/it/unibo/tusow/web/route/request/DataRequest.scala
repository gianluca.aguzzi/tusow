/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web.route.request

import io.vertx.core.http.HttpMethod
import io.vertx.scala.ext.web.RoutingContext

protected sealed trait Methods

/**
  * The basic structure of a data request.
  */
abstract class DataRequest {
  val method: HttpMethod
  val payload: String
}

/**
  * Defines a Json data request.
  *
  * @param method  the HTTP method
  * @param payload the request's payload
  */
case class JsonRequest(method: HttpMethod, payload: String) extends DataRequest

/**
  * Defines a String data request.
  *
  * @param method  the HTTP method
  * @param payload the request's payload
  */
case class StringRequest(method: HttpMethod, payload: String) extends DataRequest

/**
  * Defines an empty data request. Used only in case of errors.
  *
  * @param method  the HTTP method
  * @param payload the request's payload
  */
case class NullRequest(method: HttpMethod = null, payload: String = "") extends DataRequest

object DataRequest {
  /**
    * HTTP Content-Type header value for Json payload.
    */
  private val JSON_CONTENT_TYPE: String = "application/json"
  /**
    * HTTP Content-Type header value for String payload.
    */
  private val STRING_CONTENT_TYPE: String = "text/plain"

  /**
    * Used to encapsulate data extracted from the Vertx context.
    *
    * @param contentType the Content-Type header value
    * @param method      the HTTP method
    * @param payload     the request's payload
    */
  private case class ContextInfo(contentType: String, method: HttpMethod, payload: String)

  def apply(routingContext: RoutingContext): DataRequest = {
    val info = extractInfoFromContext(routingContext)
    requestBasedOn(info)
  }

  /**
    * Extracts useful info from the Vertx context, storing it in a ContextInfo object.
    *
    * @param routingContext the Vertx routing context
    * @return the context info
    */
  private def extractInfoFromContext(routingContext: RoutingContext): ContextInfo = {
    val req = routingContext.request()
    val payload = routingContext.getBodyAsString().getOrElse("")
    val contentType = req.getHeader("Accept").getOrElse("")
    val method = req.method()

    ContextInfo(contentType, method, payload)
  }

  /**
    * Constructs a DataRequest based on the given ContextInfo.
    * @param info the context info
    * @return a new DataRequest
    */
  private def requestBasedOn(info: ContextInfo): DataRequest = info.contentType match {
    case JSON_CONTENT_TYPE => JsonRequest(info.method, info.payload)
    case STRING_CONTENT_TYPE => StringRequest(info.method, info.payload)
    case _ => NullRequest()
  }
}

