/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web.route.handler

import java.io.IOException
import java.net.URLDecoder

import io.vertx.core.Handler
import io.vertx.scala.ext.web.{Router, RoutingContext}
import it.unibo.tusow.web.C
import it.unibo.tusow.web.DataSpaceHttpProxy._
import it.unibo.tusow.web.StatusCodes._
import it.unibo.tusow.web.controller.DataSpaceController

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

/**
  * Intercepts and handles HTTP requests directed towards a specific type of data spaces.
  * <br>
  * If a request addresses a data space whose name is not known to this handler,
  * a new data space with the requested name is created.
  *
  * @param executionContext the concurrent execution context
  * @tparam D the type of data contained in the handled data spaces
  * @tparam T the type of the template used to query the data spaces.
  */
abstract class DataSpaceRouteHandler[D, T](implicit executionContext: ExecutionContext) {

  private var dataSpaceMap: collection.mutable.Map[String, DataSpaceController[D, T]] =
    collection.mutable.Map[String, DataSpaceController[D, T]]()

  def bindTo(router: Router, route: String): Unit = {
    router.route(route).handler(handleNewDataSpace).handler(_.next())
    router.get(route)
      .produces(contentType)
      .handler(handleGet)
      .handler(_.next())
    router.post(route)
      .consumes(acceptsDatum)
      .produces(contentType)
      .handler(handlePost)
      .handler(_.next())
    router.put(route)
      .consumes(acceptsDatum)
      .produces(contentType)
      .handler(handlePut)
      .handler(_.next())
    router.delete(route).
      produces(contentType)
      .handler(handleDelete)
      .handler(_.next())
  }

  /**
    * Creates a new controller for a newly requested data space.
    *
    * @return the new data space.
    */
  protected def spawnNewController(): DataSpaceController[D, T]

  /**
    * Defines the content type produced by this handler.
    *
    * @return the Content-Type header.
    */
  protected def contentType: String

  /**
    * Defines the content type consumed by this handler.
    * @return the consumed content type
    */
  protected def acceptsDatum: String

  /**
    * Defines the separator used to split a payload containing multiple data.
    * @return the payload's data separator
    */
  protected def dataSeparator: String = C.defaults.sep

  /**
    * Defines a mapping between the textual representation of a datum and its representation
    * in the format `D`
    * @param payloadDatum any single datum contained in the payload
    * @return the representation of type `D` of the datum.
    */
  protected def payloadToDatum(payloadDatum: String): D

  protected def payloadToData(payloadDatum: String): Seq[D]

  private def parsePayload(bulk: Boolean, payload: String): Option[Seq[D]] = {
    try {
      if (bulk) {
        Some(payloadToData(payload))
      } else {
        Some(Seq(payloadToDatum(payload)))
      }
    } catch {
      case io: Exception => None
    }
  }

  /**
    * Defines a mapping between the data returned by some data space controller
    * and the same data in a String format - thus capable of being sent as
    * an HTTP response payload.
    *
    * @param optDatum the representation of type D of the response data
    * @return the textual representation of the the response data.
    */
  protected def dataToString(optDatum: Seq[D]): String

  /**
    * Defines a mapping between the URI representation of a template
    * and a template format intelligible by the handled data space controllers.<br>
    * This method follows the <i>template method</i> pattern as the variant, implementation-specific
    * code run by the `parseQuery` method.
    *
    * @param query the query sent through the request URI
    * @return the representation of type `T` of the template.
    */
  protected def queryToTemplate(query: String): T

  /**
    * Safely converts the URI representation of a template to the format `T`.<br>
    * This method follows the <i>template method</i> pattern as the invariant (base) portion of code.
    * @param query
    * @return the representation of type `T` of the template.
    */
  private def parseQuery(query: String): T = if (!query.isEmpty) queryToTemplate(query) else null.asInstanceOf[T]

  /**
    * Vertx handler for creating a new data space controller for the requested data space name.
    *
    * @return the next Vertx handler.
    */
  private def handleNewDataSpace(): Handler[RoutingContext] = ctx => {
    val dsName = ctx.request().getParam(PARAM_DS).getOrElse("")
    if (!dataSpaceMap.contains(dsName)) {
      dataSpaceMap += (dsName -> spawnNewController())
    }
    ctx.next()
  }

  /**
    * Handles an incoming HTTP GET request.
    *
    * @return a GET handler.
    */
  protected def handleGet(): Handler[RoutingContext] = ctx => {
    val controller = getController(ctx)
    val template = getTemplate(ctx)
    val predicative = isPredicative(ctx)
    val bulk = isBulk(ctx)
    controller.getFromDataSpace(Option(parseQuery(URLDecoder.decode(template, "UTF-8"))), bulk, predicative)
      .onComplete {
        case Success(l) => ctx.response()
          // TODO redundant?
          .putHeader(CONTENT_TYPE_HEADER, contentType)
          .setStatusCode(if (l.nonEmpty) OK else NO_CONTENT)
          .end(dataToString(l))
        case Failure(_) => ???
      }
  }

  /**
    * Handles an incoming HTTP POST request.
    *
    * @return a POST handler.
    */
  protected def handlePost(): Handler[RoutingContext] = ctx => {
    val controller = getController(ctx)
    val body = getBody(ctx)
    val sync = isSynchronous(ctx)
    val bulk = isBulk(ctx)

    val payload = parsePayload(bulk, body)
    if (payload.isDefined) {
      controller.postOnDataSpace(payload.get, bulk, sync)
        .onComplete {
          case Success(o) => ctx.response()
            .putHeader(CONTENT_TYPE_HEADER, contentType)
            .setStatusCode(if (sync) CREATED else ACCEPTED)
            .end(dataToString(o))
          case Failure(e) => ???
        }
    } else {
      ctx.response()
        .setStatusCode(400)
        .end()
    }
  }

  /**
    * Handles an incoming HTTP PUT request.
    *
    * @return a PUT handler.
    */
  protected def handlePut(): Handler[RoutingContext] = ctx => {
    val controller = getController(ctx)
    val body = getBody(ctx)
    val sync = isSynchronous(ctx)
    val bulk = isBulk(ctx)

    val payload = parsePayload(bulk, body)
    if (payload.isDefined) {
      controller.putOnDataSpace(payload.get, sync)
        .onComplete {
          case Success(o) => ctx.response()
            .putHeader(CONTENT_TYPE_HEADER, contentType)
            .setStatusCode(if (sync) CREATED else ACCEPTED)
            .end(dataToString(o))
          case Failure(_) => ???
        }
    } else {
      ctx.response()
        .setStatusCode(400)
        .end()
    }
  }

  /**
    * Handles an incoming HTTP DELETE request.
    *
    * @return a DELETE handler.
    */
  protected def handleDelete(): Handler[RoutingContext] = ctx => {
    val controller = getController(ctx)
    val template = getTemplate(ctx)
    val predicative = isPredicative(ctx)
    val bulk = isBulk(ctx)

    controller.deleteFromDataSpace(Option(parseQuery(URLDecoder.decode(template, "UTF-8"))), bulk, predicative)
      .onComplete {
        case Success(l) => ctx.response()
          .putHeader(CONTENT_TYPE_HEADER, contentType)
          .setStatusCode(if (l.nonEmpty) OK else NO_CONTENT)
          .end(dataToString(l))
        case Failure(_) => ???
      }
  }

  /**
    * Returns the data space controller corresponding to the data space name
    * specified in the incoming request URI.
    *
    * @param ctx the Vertx routing context
    * @return the requested data space controller.
    */
  protected final def getController(ctx: RoutingContext): DataSpaceController[D, T] = {
    val dsName = ctx.request().getParam(PARAM_DS).getOrElse("")
    dataSpaceMap(dsName)
  }

  /**
    * Extracts the 'template' parameter value from an incoming GET/DELETE request URI.
    *
    * @param ctx the Vertx routing context
    * @return the template.
    */
  private final def getTemplate(ctx: RoutingContext): String = {
    URLDecoder.decode(ctx.request().getParam(PARAM_TEMPLATE).getOrElse(""), "UTF-8")
  }

  /**
    * Extracts the 'predicative' parameter value from an incoming GET/DELETE request URI
    * as a boolean value.
    *
    * @param ctx the Vertx routing context
    * @return whether the request is predicative.
    */
  private final def isPredicative(ctx: RoutingContext): Boolean =
    ctx.request().getParam(C.keys.predicative)
      .map(java.lang.Boolean.parseBoolean)
      .getOrElse(false)

  /**
    * Extracts the 'bulk' parameter value from a request URI
    * as a boolean value.
    *
    * @param ctx the Vertx routing context
    * @return whether the request is bulk.
    */
  protected final def isBulk(ctx: RoutingContext): Boolean =
    ctx.request().getParam(C.keys.bulk)
      .map(java.lang.Boolean.parseBoolean)
      .getOrElse(false)

  /**
    * Extracts the 'sync' parameter value from an incoming POST request URI
    * as a boolean value.
    *
    * @param ctx the Vertx routing context
    * @return whether the request is synchronous.
    */
  protected def isSynchronous(ctx: RoutingContext): Boolean = {
    ctx.request().getParam(C.keys.sync)
      .map(java.lang.Boolean.parseBoolean)
      .getOrElse(true)
  }

  /**
    * Extracts the body from an incoming POST request.
    *
    * @param ctx the Vertx routing context
    * @return the request body
    */
  protected def getBody(ctx: RoutingContext): String = ctx.getBodyAsString().getOrElse("")

  private[handler] def getData(dsName: String): Future[List[D]] = {
    dataSpaceMap.get(dsName) match {
      case Some(controller) => controller.getData
      case None =>
        val newController = spawnNewController()
        dataSpaceMap.put(dsName, newController)
        newController.getData
    }
  }

  private[handler] def addData(dsName: String) = ???

  private[handler] def deleteData(dsName: String) = ???

}
