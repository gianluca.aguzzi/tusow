package it.unibo.tusow.web.route.handler

import alice.tuprolog.{Struct, Term}
import it.unibo.tusow.PrologAsyncDataSpace
import it.unibo.tusow.web.C
import it.unibo.tusow.web.controller.{DataSpaceController, PrologDataSpaceController}
import it.unibo.utils.prolog.Extensions._

import scala.collection.mutable
import scala.concurrent.ExecutionContext

class PrologRequestHandler(implicit executionContext: ExecutionContext) extends DataSpaceRouteHandler[Term, Term] {
  override protected def spawnNewController(): DataSpaceController[Term, Term] =
    PrologDataSpaceController(PrologAsyncDataSpace())

  override protected def contentType: String = "application/prolog"

  override protected def acceptsDatum: String = "application/prolog"

  override protected def dataSeparator: String = C.defaults.sep

  override protected def payloadToDatum(payloadDatum: String): Term = payloadDatum.toPrologTerm

  override protected def dataToString(optDatum: Seq[Term]): String = optDatum.mkString("[", ",", "]")

  override protected def queryToTemplate(query: String): Term = query.toPrologGoal

  override protected def payloadToData(payloadDatum: String): Seq[Term] = {
    val term: Term = payloadDatum.toPrologTerm
    if (term.isList) {
      prologListToSeq(term.asInstanceOf[Struct])
    } else {
      Seq(term)
    }
  }

  private def prologListToSeq(xs: Struct): Seq[Term] = {
    val result : mutable.Buffer[Term] = mutable.ListBuffer[Term]()
    xs.listIterator().forEachRemaining((t: Term) => result.append(t))
    result
  }
}
