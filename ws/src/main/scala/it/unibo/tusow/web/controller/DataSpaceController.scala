/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web.controller

import it.unibo.tusow.AsyncDataSpace

import scala.concurrent.Future

/**
  * Provides a mapping between the HTTP methods used by
  * <a href="https://app.swaggerhub.com/apis/lrizzato/TuSoW/1.0.0">TuSoW REST API</a>
  * and the operations available on some data space.
  * <br>
  * Since different data space types behave uniformly to each other, implementor classes need only
  * provide a concrete binding to the [[Datum]] and [[Template]] generic types.
  *
  * @param ds the controlled data space
  * @tparam Datum    the type of data contained in the data space
  * @tparam Template the type of the template used to query the data space
  */
abstract class DataSpaceController[Datum, Template](ds: AsyncDataSpace[Datum, Template]) {

  import scala.concurrent.ExecutionContext.Implicits.global

  /**
    * Makes a <code>read</code> request on the controlled dataspace, returning
    * a matching [[Datum]] asynchronously.
    *
    * @param template    the template used to query the data space
    * @param predicative whether this controller should return immediately when no matching
    *                    data is available
    * @return a Future containing Some(Datum) if the request succeeded,
    *         None otherwise
    */
  def getFromDataSpace(template: Option[Template], bulk: Boolean, predicative: Boolean): Future[List[Datum]] = {
    if (template.isDefined) {
      if (bulk) {
        ds.readAll(template.get)
      } else if (predicative) {
        ds.readIfPresent(template.get).map(_.toList)
      } else {
        ds.read(template.get).map(List(_))
      }
    } else {
      ds.get()
    }
  }

  /**
    * * Makes a <code>put</code> request on the controlled dataspace, returning
    * * the inserted [[Datum]] asynchronously.
    *
    * @param data the datum to be inserted
    * @param sync whether this controller should wait for the completed insertion
    * @return the inserted datum
    */
  def postOnDataSpace(data: Seq[Datum], bulk: Boolean, sync: Boolean): Future[Seq[Datum]] = {
    if (bulk) {
      ds.putAll(data)
    } else if (sync) {
      ds.put(data.head).map(Seq(_))
    } else {
      ds.put(data.head)
      Future {
        data
      }
    }
  }

  def putOnDataSpace(data: Seq[Datum], sync: Boolean): Future[Seq[Datum]] = {
    if (sync) {
      ds.set(data)
    } else {
      ds.set(data)
      Future {
        data
      }
    }
  }

  /**
    * Makes a <code>take</code> request on the controlled dataspace, returning
    * a matching [[Datum]] asynchronously.
    *
    * @param template    the template used to query the data space
    * @param predicative whether this controller should return immediately when no matching
    *                    data is available
    * @return a Future containing Some(Datum) if the request succeeded,
    *         None otherwise
    */
  def deleteFromDataSpace(template: Option[Template], bulk: Boolean, predicative: Boolean): Future[List[Datum]] = {
    if (template.isDefined) {
      if (bulk) {
        ds.takeAll(template.get)
      } else if (predicative) {
        ds.takeIfPresent(template.get).map(_.toList)
      } else {
        ds.take(template.get).map(List(_))
      }
    } else {
      ds.clear()
    }
  }

  def getData: Future[List[Datum]] = ds.get()

}
