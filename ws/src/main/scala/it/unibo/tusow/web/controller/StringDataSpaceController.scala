/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web.controller

import it.unibo.tusow.AsyncDataSpace

import scala.util.matching.Regex

/**
  * This Controller uses [[String]] as the data format,
  * and [[String]] as the templating language.
  *
  * @param ds the controlled data space
  */
class StringDataSpaceController(ds: AsyncDataSpace[String, Regex]) extends DataSpaceController[String, Regex](ds) {
}

object StringDataSpaceController {
  def apply(controller: AsyncDataSpace[String, Regex]): DataSpaceController[String, Regex] = new StringDataSpaceController(controller)
}
