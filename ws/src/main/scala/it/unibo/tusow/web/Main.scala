/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web

import io.vertx.core.{AsyncResult, Handler}
import io.vertx.lang.scala.ScalaVerticle
import io.vertx.scala.core.{DeploymentOptions, Vertx}
import org.slf4j.LoggerFactory

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future, Promise, duration}
import scala.util.{Failure, Success}

/**
  * TuSoW server entry point. Run this app to start a TuSoW server.
  */
object Main extends App {

  val L = LoggerFactory.getLogger(this.getClass)

  val vertx = Vertx.vertx();

  val actualArgs = TusowArgs(args)

  if (actualArgs.isEmpty) {
    System.exit(0)
  }

  val deployID: Future[String] = vertx
    .deployVerticleFuture(ScalaVerticle.nameForVerticle[DataSpaceHttpProxy], actualArgs.get.toDeploymentOptions)

  deployID.onComplete({
    case Success(_) => L.info("TuSoW service successfully deployed")
    case Failure(_) => L.error("Error while deploying TuSoW service")
  })

  def terminate(): Unit = {
    val id = Await.result(deployID, Duration.Inf)

    val termination = vertx.undeployFuture(id)

    termination.onComplete{
      case Success(_) => L.info("TuSoW service successfully terminated")
      case Failure(e) => {
        L.error(s"Error while terminating TuSoW service", e)
      }
    }

    Await.ready(termination, Duration.Inf)
  }

  Await.ready(deployID, Duration.Inf)
}
