package it.unibo.tusow.web.controller

import alice.tuprolog.Term
import it.unibo.tusow.AsyncDataSpace

class PrologDataSpaceController(ds: AsyncDataSpace[Term, Term]) extends DataSpaceController[Term, Term](ds) {
}

object PrologDataSpaceController {
  def apply(ds: AsyncDataSpace[Term, Term]): DataSpaceController[Term, Term] = new PrologDataSpaceController(ds)
}
