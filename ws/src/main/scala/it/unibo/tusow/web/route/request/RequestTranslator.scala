/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web.route.request

/**
  * This class is capable of converting a request from some type to
  * a base type, acting as a neutral representation.
  * <br>
  * This works as a chain of responsibility: if some translator cannot
  * convert a data format, it delegates the translation process to the next
  * one in the translation chain, in a recursive fashion. Thus, a RequestTranslator
  * should not be instantiated on its own: you should use the companion's <code>apply()</code>
  * method to get a reference to the chain instance.
  *
  * @see RequestTranslator's companion object
  */
abstract class RequestTranslator {

  /**
    * The base type to which all request should be traslated into.
    */
  protected type BASE_TYPE = JsonRequest

  var successor: RequestTranslator = null

  def translate(dataRequest: DataRequest): DataRequest = {
    if (mayTranslate(dataRequest)) {
      convertRequest(dataRequest)
    } else if (successor != null) {
      successor.translate(dataRequest)
    } else {
      NullRequest()
    }
  }

  def setSuccessor(successor: RequestTranslator): Unit = this.successor = successor

  /**
    * Checks whether this translator is capable of translating the given request, based
    * on its type.
    *
    * @param dataRequest the request to be translated
    * @return whether this translator can perform the translation operation.
    */
  protected def mayTranslate(dataRequest: DataRequest): Boolean

  /**
    * Converts the given request to the base type.
    *
    * @param dataRequest the request to be translated
    * @return the translated request.
    */
  protected def convertRequest(dataRequest: DataRequest): BASE_TYPE

}

/**
  * RequestTranslator's companion object. Client code should use this to
  * get a reference to the default translation chain.
  */
object RequestTranslator {

  private var firstTranslator: RequestTranslator = null

  def apply(): RequestTranslator = {
    if (firstTranslator == null) {
      firstTranslator = generateTranslationChain()
    }
    firstTranslator
  }

  private def generateTranslationChain(): RequestTranslator = {
    val translator = StringTranslator()
    translator.setSuccessor(JsonTranslator())
    translator
  }
}
