/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web.route.request

import io.vertx.core.http.HttpMethod

/**
  * A translator capable of translating a String data request.
  */
class StringTranslator extends RequestTranslator {
  override protected def mayTranslate(dataRequest: DataRequest): Boolean = dataRequest.isInstanceOf[StringRequest]

  override protected def convertRequest(dataRequest: DataRequest): BASE_TYPE = {
    val newPayload = dataRequest.method match {
      case HttpMethod.POST => s"""{"string":"${dataRequest.payload}"}"""
      case HttpMethod.GET | HttpMethod.DELETE => "$" + s"""..[?(@.string == ${dataRequest.payload})]"""
      case _ => ""
    }
    JsonRequest(dataRequest.method, newPayload)
  }
}

object StringTranslator {
  def apply(): StringTranslator = new StringTranslator()
}
