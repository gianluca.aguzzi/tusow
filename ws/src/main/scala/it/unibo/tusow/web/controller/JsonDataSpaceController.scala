/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web.controller

import com.fasterxml.jackson.databind.JsonNode
import com.jayway.jsonpath.JsonPath
import it.unibo.tusow.AsyncDataSpace

/**
  * This Controller uses {@link net.minidev.json.JSONObject} as the data format,
  * and {@link com.jayway.jsonpath.JsonPath} as the templating language.
  *
  * @param ds the controlled data space
  */
class JsonDataSpaceController(ds: AsyncDataSpace[JsonNode, JsonPath]) extends DataSpaceController[JsonNode, JsonPath](ds) {
}

object JsonDataSpaceController {
  def apply(ds: AsyncDataSpace[JsonNode, JsonPath]): DataSpaceController[JsonNode, JsonPath] = new JsonDataSpaceController(ds)
}
