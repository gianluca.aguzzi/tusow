/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web.route.handler

import it.unibo.tusow.StringAsyncDataSpace
import it.unibo.tusow.web.C
import it.unibo.tusow.web.controller.{DataSpaceController, StringDataSpaceController}

import scala.concurrent.ExecutionContext
import scala.util.matching.Regex

/**
  * Intercepts and handles HTTP requests directed towards String data spaces.
  * <br>
  * If a request addresses a data space whose name is not known to this handler,
  * a new data space with the requested name is created.
  *
  * @param executionContext the concurrent execution context
  */
class StringRequestHandler(implicit executionContext: ExecutionContext)
  extends DataSpaceRouteHandler[String, Regex] {

  override protected def spawnNewController(): DataSpaceController[String, Regex] =
    StringDataSpaceController(StringAsyncDataSpace())

  override protected def contentType: String = "text/plain"

  override protected def acceptsDatum: String = "text/plain"

  override protected def dataSeparator: String = C.defaults.sep

  override protected def payloadToDatum(payloadDatum: String): String = payloadDatum

  override protected def dataToString(optDatum: Seq[String]): String = optDatum.mkString("\n")

  override protected def queryToTemplate(query: String): Regex = query.r

  override protected def payloadToData(payloadDatum: String): Seq[String] = payloadDatum.split("\n")
}
