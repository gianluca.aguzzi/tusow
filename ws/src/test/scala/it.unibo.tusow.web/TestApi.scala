/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web

import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpHeaders
import io.vertx.lang.scala.ScalaVerticle
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.{HttpResponse, WebClient}
import it.unibo.utils.concurrency.AwaitUtils._
import org.scalatest.{AsyncFlatSpec, Matchers}

import scala.concurrent.duration.Duration
import scala.concurrent.{Future, TimeoutException, duration}

class TestApi extends AsyncFlatSpec with Matchers {

  val vertx = Vertx.vertx()

  val client = WebClient.create(vertx)

  val port = 8080
  val host = "localhost"

  await(vertx.deployVerticleFuture(ScalaVerticle.nameForVerticle[DataSpaceHttpProxy]))

  "A GET request" should "retrieve matching data from the data space" in {
    val dsName = "getDS"

    val postReq = await(FOR_A_LONG_WHILE)(postRequest(dsName, "Lorenzo", false))
    val postRes = postReq.bodyAsString.get
    postRes should be("Lorenzo")

    val getReq = await(getRequest("getDS", ".*?oren.*", false))
    val getRes = getReq.bodyAsString.get
    getReq.statusCode() should be (StatusCodes.OK)
    getRes should be("Lorenzo")
  }

  it should "suspend until matching data is present in the data space" in {
    val dsName = "getBlockingDS"

    val futureGetReq = getRequest(dsName, ".*?oren.*", false)
    a[TimeoutException] should be thrownBy await(FOR_A_LONG_WHILE)(futureGetReq)
    futureGetReq.isCompleted should be (false)

    val postReq = await(FOR_A_LONG_WHILE)(postRequest(dsName, "Lorenzo", false))
    val postRes = postReq.bodyAsString.get
    postRes should be("Lorenzo")

    val getReq = await(futureGetReq)
    val getRes = getReq.bodyAsString.get
    getReq.statusCode() should be (StatusCodes.OK)
    getRes should be ("Lorenzo")
    futureGetReq.isCompleted should be (true)
  }

  "A predicative GET request" should "succeed if matching data is present in the data space" in {
    val dsName = "getPredDS"

    val postReq = await(FOR_A_LONG_WHILE)(postRequest(dsName, "Lorenzo", false))
    val postRes = postReq.bodyAsString.get
    postRes should be("Lorenzo")

    val getReq = await(getRequest(dsName, ".*?oren.*", true))
    val getRes = getReq.bodyAsString.get
    getReq.statusCode() should be (StatusCodes.OK)
    getRes should be("Lorenzo")
  }

  it should "fail if no matching data is present in the data space" in {
    val dsName = "getPredFailDS"

    val futureGetReq = getRequest(dsName, ".*?oren.*", true)
    val getReq = await(futureGetReq)

    getReq.statusCode() should be (StatusCodes.NO_CONTENT)
    futureGetReq.isCompleted should be (true)
  }

  "A POST request" should "insert new data asynchronously in the data space" in {
    val dsName = "postDS"

    val postReq = await(postRequest(dsName, "Lorenzo", false))
    val postRes = postReq.bodyAsString.get
    postReq.statusCode() should be (StatusCodes.ACCEPTED)
    postRes should be ("Lorenzo")

    val getReq = await(getRequest(dsName, ".*?oren.*", false))
    val getRes = getReq.bodyAsString.get
    getReq.statusCode() should be (StatusCodes.OK)
    getRes should be ("Lorenzo")
  }

  "A synchronous POST request" should "wait for a successful insertion of new data in the data space" in {
    val dsName = "postSyncDS"

    val postReq = await(postRequest(dsName, "Lorenzo", true))
    val postRes = postReq.bodyAsString.get
    postReq.statusCode() should be (StatusCodes.CREATED)
    postRes should be ("Lorenzo")

    val getReq = await(getRequest(dsName, ".*?oren.*", false))
    val getRes = getReq.bodyAsString.get
    getReq.statusCode() should be (StatusCodes.OK)
    getRes should be ("Lorenzo")
  }

  "A DELETE request" should "remove matching data present in the data space" in {
    val dsName = "deleteDS"

    val postReq = await(postRequest(dsName, "Lorenzo", false))
    val postRes = postReq.bodyAsString.get
    postRes should be ("Lorenzo")

    val deleteReq = await(deleteRequest(dsName, ".*?oren.*", false))
    val deleteRes = deleteReq.bodyAsString.get
    deleteReq.statusCode() should be (StatusCodes.OK)
    deleteRes should be ("Lorenzo")
  }

  it should "suspend until matching data is present in the data space" in {
    val dsName = "deleteBlockingDS"

    val futureDeleteReq = deleteRequest(dsName, ".*?oren.*", false)
    a [TimeoutException] should be thrownBy await(FOR_A_LONG_WHILE)(futureDeleteReq)
    futureDeleteReq.isCompleted should be (false)

    val postReq = await(FOR_A_LONG_WHILE)(postRequest(dsName, "Lorenzo", false))
    val postRes = postReq.bodyAsString.get
    postRes should be("Lorenzo")

    val deleteReq = await(futureDeleteReq)
    val deleteRes = deleteReq.bodyAsString.get
    deleteReq.statusCode() should be (StatusCodes.OK)
    deleteRes should be ("Lorenzo")
    futureDeleteReq.isCompleted should be (true)
  }

  "A predicative DELETE request" should "succeed if matching data is present in the data space" in {
    val dsName = "deletePredDS"

    val postReq = await(FOR_A_LONG_WHILE)(postRequest(dsName, "Lorenzo", false))
    val postRes = postReq.bodyAsString.get
    postRes should be("Lorenzo")

    val deleteReq = await(getRequest(dsName, ".*?oren.*", true))
    val deleteRes = deleteReq.bodyAsString.get
    deleteRes should be("Lorenzo")
  }

  it should "fail if no matching data is present in the data space" in {
    val dsName = "deletePredFailDS"

    val futureDeleteReq = deleteRequest(dsName, ".*?oren.*", true)
    val deleteReq = await(futureDeleteReq)

    deleteReq.statusCode() should be (StatusCodes.NO_CONTENT)
    futureDeleteReq.isCompleted should be (true)
  }

  def getRequest(dsName: String, template: String, predicative: Boolean): Future[HttpResponse[Buffer]] = {
    client.get(port, host, s"${C.defaults.root}/string/" + dsName)
      .addQueryParam("template", template)
      .addQueryParam("predicative", predicative.toString)
      .putHeader(HttpHeaders.ACCEPT.toString, "text/plain")
      .sendFuture()
  }

  def postRequest(dsName: String, payload: String, sync: Boolean): Future[HttpResponse[Buffer]] = {
    client.post(port, host, f"${C.defaults.root}/string/" + dsName)
      .addQueryParam("sync", sync.toString)
      .putHeader(HttpHeaders.ACCEPT.toString, "text/plain")
      .putHeader(HttpHeaders.CONTENT_TYPE.toString, "text/plain")
      .sendBufferFuture(Buffer.buffer(payload))
  }

  def deleteRequest(dsName: String, template: String, predicative: Boolean): Future[HttpResponse[Buffer]] = {
    client.delete(port, host, "/data_spaces/string/" + dsName)
      .addQueryParam("template", template)
      .addQueryParam("predicative", predicative.toString)
      .putHeader(HttpHeaders.ACCEPT.toString, "text/plain")
      .sendFuture()
  }

  val FOR_A_WHILE: Duration = Duration(1, duration.SECONDS)

  val FOR_A_LONG_WHILE: Duration = Duration(3, duration.SECONDS)

}
