/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web.client

import it.unibo.utils.concurrency.AwaitUtils._

import scala.io.StdIn

object Main extends App {

  val argumentRegex = """(?:\"(.*?[^\\])\")|(?:\'(.*?[^\\])\')|([^\s]+)""".r
  val defaults: Option[TusowClientArgs] = TusowClientArgs(args).orElse(Some(TusowClientArgs()))

  var current: Option[TusowClientArgs] = defaults
  var previous: Option[TusowClientArgs] = None

  for (round <- 1 to Int.MaxValue) {
    handleInput(current)
    previous = current.map(_.resetOperation()).orElse(Some(TusowClientArgs()))
    current = TusowClientArgs(getInput(s"[$round] ${current.map(_.toURI()).get}"), previous.get).orElse(previous)
  }

  def getInput(left: String = ""): Seq[String] = {
    val line = StdIn.readLine(s"$left > ")
    getArgs(line)
  }

  def handleInput(args: Option[TusowClientArgs]): Unit = {
    // TODO handle all cases
    if (args.isEmpty)
      println("Bad command")
    else {
      val it = args.get
      it match {
        case TusowClientArgs(_, _, _, _, Help, _, _, _, _) =>
          println(TusowClientArgs.help())
        case TusowClientArgs(_, _, f, _, Put, d, _, b, s) =>
          println(await(DataSpaceClient.setFormat(f).setUrl(it.toURI()).put(d, s, b)))
        case TusowClientArgs(_, _, f, _, Set, d, _, _, s) =>
          println(await(DataSpaceClient.setFormat(f).setUrl(it.toURI()).set(d, s)))
        case TusowClientArgs(_, _, f, _, Read, t, p, b, _) =>
          println(await(DataSpaceClient.setFormat(f).setUrl(it.toURI()).read(t, p, b)))
        case TusowClientArgs(_, _, f, _, Take, t, p, b, _) =>
          println(await(DataSpaceClient.setFormat(f).setUrl(it.toURI()).take(t, p, b)))
        case TusowClientArgs(_, _, f, _, Get, _, _, _, _) =>
          println(await(DataSpaceClient.setFormat(f).setUrl(it.toURI()).read()))
        case TusowClientArgs(_, _, f, _, Clear, _, _, _, _) =>
          println(await(DataSpaceClient.setFormat(f).setUrl(it.toURI()).take()))
        case _ =>

      }
    }
  }

  def getArgs(string: String): Seq[String] = {
    argumentRegex.findAllMatchIn(string)
      .flatMap(m => m.group(1) :: m.group(2) :: m.group(3) :: Nil)
      .filterNot(_ == null)
      .toStream
  }
}
