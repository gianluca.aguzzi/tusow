/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web.client

import java.net.{URL, URLEncoder}

import io.vertx.core.buffer.Buffer
import io.vertx.lang.scala.VertxExecutionContext
import io.vertx.scala.core.Vertx
import io.vertx.scala.ext.web.client.WebClient

import scala.concurrent.Future

object DataSpaceClient {
  private val client: WebClient = WebClient.create(Vertx.vertx())
  private implicit val executionContext: VertxExecutionContext = VertxExecutionContext(Vertx.vertx().getOrCreateContext())

  private var format: Format = Str
  private var url: URL = new URL(s"${C.defaults.url}/${format.path}/${C.defaults.dataSpace}")

  def setFormat(format: Format): this.type = {
    this.format = format
    this
  }

  def setUrl(url: URL): this.type = {
    this.url = url
    this
  }

  def put(datum: String, sync: Boolean = true, bulk: Boolean = false): Future[String] = {
    client.postAbs(url.toString)
      .addQueryParam("sync", sync.toString)
      .addQueryParam("bulk", bulk.toString)
      .putHeader("Content-Type", format match {
        case Str => "text/plain"
        case Json => "application/json"
        case Prolog => "text/plain"
      })
      .putHeader("Accept", format match {
        case Str => "text/plain"
        case Json => "application/json"
        case Prolog => "text/plain"
      })
      .putHeader("Connection", "keep-alive")
      .sendBufferFuture(Buffer.buffer(datum))
      .map(_.bodyAsString())
      .map(_.getOrElse(""))
  }

  def set(datum: String, sync: Boolean = true): Future[String] = {
    client.putAbs(url.toString)
      .addQueryParam("sync", sync.toString)
      .putHeader("Content-Type", format match {
        case Str => "text/plain"
        case Json => "application/json"
        case Prolog => "text/plain"
      })
      .putHeader("Accept", format match {
        case Str => "text/plain"
        case Json => "application/json"
        case Prolog => "text/plain"
      })
      .putHeader("Connection", "keep-alive")
      .sendBufferFuture(Buffer.buffer(datum))
      .map(_.bodyAsString())
      .map(_.getOrElse(""))
  }

  def take(template: String = "", predicative: Boolean = false, bulk: Boolean = false): Future[String] = {
    client.deleteAbs(url.toString)
      .addQueryParam("template", URLEncoder.encode(template, "UTF-8"))
      .addQueryParam("predicative", predicative.toString)
      .addQueryParam("bulk", bulk.toString)
      .putHeader("Accept", format match {
        case Str => "text/plain"
        case Json => "application/json"
        case Prolog => "text/plain"
      })
      .putHeader("Connection", "keep-alive")
      .sendFuture()
      .map(_.bodyAsString())
      .map(_.getOrElse(""))
  }

  def read(template: String = "", predicative: Boolean = false, bulk: Boolean = false): Future[String] = {
    client.getAbs(url.toString)
      .addQueryParam("template", URLEncoder.encode(template, "UTF-8"))
      .addQueryParam("predicative", predicative.toString)
      .addQueryParam("bulk", bulk.toString)
      .putHeader("Accept", format match {
        case Str => "text/plain"
        case Json => "application/json"
        case Prolog => "text/plain"
      })
      .sendFuture()
      .map(_.bodyAsString())
      .map(_.getOrElse(""))
  }
}
