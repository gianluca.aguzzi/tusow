/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow.web.client

import java.net.URL

import scopt.OptionParser


case class TusowClientArgs(
                            url: String = C.defaults.url,
                            port: Int = C.defaults.port,
                            format: Format = C.defaults.format,
                            dataSpace: String = C.defaults.dataSpace,
                            operation: Operation = null,
                            argument: String = null,
                            predicative: Boolean = false,
                            bulk: Boolean = false,
                            sync: Boolean = true
                          ) {

  def resetOperation(): TusowClientArgs =
    copy(operation = null, argument = null, predicative = false)

  def toURI(): URL = {
    val tmp = new URL(s"$url")
    new URL(tmp.getProtocol, tmp.getHost, port, s"${tmp.getPath}${format.path}/$dataSpace")
  }
}

object TusowClientArgs {

  // https://github.com/scopt/scopt
  private lazy val parser = new OptionParser[TusowClientArgs]("TuSoW CLI") {
    head("TuSoW CLI - Tuple Spaces over the Web Command Line Interface")

    cmd("put")
      .text("Puts a datum on the data space")
      .action((_, args) => args.copy(operation = Put))
      .children(
        arg[String]("datum").required().text("The datum to be put on the dataspace").action((d, args) => args.copy(argument = d)),
        opt[Unit]('S', "synchronous").optional().action((_, args) => args.copy(sync = true)),
        opt[Unit]('B', "bulk").optional().action((_, args) => args.copy(bulk = true))
      )

    cmd("set")
      .text("Replaces all data in the data space")
      .action((_, args) => args.copy(operation = Set))
      .children(
        arg[String]("datum").required().text("The datum to be put on the dataspace").action((d, args) => args.copy(argument = d)),
        opt[Unit]('S', "synchronous").optional().action((_, args) => args.copy(sync = true)),
      )

    cmd("take")
      .text("Takes a datum from the data space")
      .action((_, args) => args.copy(operation = Take))
      .children(
        arg[String]("template").required().text("The template of the datum to be retrieved").action((d, args) => args.copy(argument = d)),
        opt[Unit]('P', "predicative").optional().action((_, args) => args.copy(predicative = true)),
        opt[Unit]('B', "bulk").optional().action((_, args) => args.copy(bulk = true))
      )

    cmd("read")
      .text("Reads a datum from the data space")
      .action((_, args) => args.copy(operation = Read))
      .children(
        arg[String]("template").required().text("The template of the datum to be retrieved").action((d, args) => args.copy(argument = d)),
        opt[Unit]('P', "predicative").optional().action((_, args) => args.copy(predicative = true)),
        opt[Unit]('B', "bulk").optional().action((_, args) => args.copy(bulk = true))
      )

    cmd("get")
      .text("Reads all data from a data space")
      .action((_, args) => args.copy(operation = Get))

    cmd("clear")
      .text("Takes all data from a data space")
      .action((_, args) => args.copy(operation = Clear))

    cmd("help")
      .text("Prints this message")
      .action((_, args) => args.copy(operation = Help))

    opt[String]('u', "url")
      .text(s"Service url. Defaults to '${C.defaults.url}'")
      .optional()
      .action((u, args) => args.copy(url = u))

    opt[Int]('p', "port")
      .text(s"Service port. Defaults to ${C.defaults.port}")
      .optional()
      .action((p, args) => args.copy(port = p))

    opt[Unit]('j', "json")
      .text(s"Adopts JSON as tuple language and JSONPath as template language. Defaults to ${C.defaults.format == Json}")
      .optional()
      .action((_, args) => args.copy(format = Json))

    opt[Unit]('s', "str")
      .text(s"Adopts strings as tuple language and regexs as template language. Defaults to ${C.defaults.format == Str}")
      .optional()
      .action((_, args) => args.copy(format = Str))

    opt[Unit]('l', "prolog")
      .text(s"Adopts Prolog terms as both tuple and template language. Defaults to ${C.defaults.format == Prolog}")
      .optional()
      .action((_, args) => args.copy(format = Prolog))

    opt[String]('d', "dataSpace")
      .text(s"The data space receiving the operation. Defaults to '${C.defaults.dataSpace}'")
      .optional()
      .action((d, args) => args.copy(dataSpace = d))

    override def showUsageOnError: Boolean = false

  }

  def apply(args: Seq[String]): Option[TusowClientArgs] = parser.parse(args, TusowClientArgs())

  def apply(args: Seq[String], other: TusowClientArgs): Option[TusowClientArgs] = parser.parse(args, other)

  def help(): String = {
    parser.usage
  }
}