/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow

import com.fasterxml.jackson.databind.JsonNode
import it.unibo.tusow.SyncDataSpace.Extensions._
import it.unibo.utils.json.Extensions._

object Prova extends App {
  val ads = JsonAsyncDataSpace()
  val sds = ads.asSync()

  def tuple(i: Int): JsonNode = {
    "{\"tuple\": {\"value\": %d}}".format(i).toJsonNode
  }

//  for (i <- 0 until 100) {
//    sds.put(tuple(i))
//  }
//
//  println(ads)
//
//  for (i <- 0 until 50) {
//    sds.take("$.tuple[?(@.value > 15)]".toJsonPath)
//  }
//
//  println(ads)

  println(sds.put(tuple(0)))
  println(sds.readIfPresent("$[\"a\"]".toJsonPath))

}
