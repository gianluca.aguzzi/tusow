/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow

import java.io.IOException

import com.jayway.jsonpath.InvalidPathException
import it.unibo.utils.json.Extensions._
import org.scalatest.{FlatSpec, Matchers}


class TestExtensions extends FlatSpec with Matchers {

  "The toJsonObj function" should "fail if the input string is not a valid JSON or YAML node" in {

    a [IOException] should be thrownBy ("{\"key\":\"value}".toJsonNode)

    a [IOException] should be thrownBy ("{\"key\":\"value\"".toJsonNode)

    a [IOException] should be thrownBy ("{value".toJsonNode)

    a [IOException] should be thrownBy ("".toJsonNode)

  }

  "The toJsonPath function" should "fail if the input string is not a valid path" in {

    a [InvalidPathException] should be thrownBy ("$.[]".toJsonPath)

    a [InvalidPathException] should be thrownBy ("$...[]".toJsonPath)

    a [InvalidPathException] should be thrownBy ("$.[field".toJsonPath)

    a [IllegalArgumentException] should be thrownBy ("".toJsonPath)

  }

}
