/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow

import alice.tuprolog.Term
import it.unibo.utils.concurrency.AwaitUtils.await
import it.unibo.utils.prolog.Extensions._
import net.minidev.json.JSONObject
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.{Await, Future, TimeoutException, duration}
import scala.concurrent.duration.Duration

class TestPrologDataSpace extends FlatSpec with Matchers {

  import scala.concurrent.ExecutionContext.Implicits.global

  "Dataspace creation" should "succeed" in {
    val ds = PrologAsyncDataSpace()
  }

  "The Put operation" should "insert a new Datum in the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val dat = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)

    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
  }

  it should "try to unlock all suspended matching read requests" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val requestList = List(
      ds.read("""name("Lorenzo")""".toPrologGoal),
      ds.read("""name(N)""".toPrologGoal),
      ds.read("""name("Giovanni")""".toPrologGoal)
    )
    val nPendingRequests = (list: List[Future[Term]]) => list map (_.isCompleted) count(!_)

    nPendingRequests(requestList) should be (3)
    requestList foreach {
      a [TimeoutException] should be thrownBy await(FOR_A_LONG_WHILE)(_)
    }
    nPendingRequests(requestList) should be (3)

    val dat = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
    await(FOR_A_LONG_WHILE)(Future.firstCompletedOf(requestList)) should be (dat)

    nPendingRequests(requestList) should be (1)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

  }

  it should "satisfy at most one matching suspended take request" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val path1 = """name("Lorenzo")""".toPrologGoal
    val path2 = """name(N)""".toPrologGoal
    val path3 = """name("Lorenzo")""".toPrologGoal
    val requestList = List(ds.take(path1), ds.take(path2), ds.read(path3))
    val nPendingRequests = (list: List[Future[Term]]) => list map (_.isCompleted) count(!_)

    nPendingRequests(requestList) should be (3)
    requestList foreach {
      a [TimeoutException] should be thrownBy await(FOR_A_LONG_WHILE)(_)
    }
    nPendingRequests(requestList) should be (3)

    val dat = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_WHILE)(Future.firstCompletedOf(requestList)) should be (dat)
    nPendingRequests(requestList) should be >= 1
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_WHILE)(Future.firstCompletedOf(requestList)) should be (dat)
    nPendingRequests(requestList) should be >= 0
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
  }

  "The Read operation" should "return a Datum matching the given template from the data space" in {
    val ds = PrologAsyncDataSpace()

    val dat = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

    val query = """name("Lorenzo")""".toPrologGoal
    await(FOR_A_LONG_WHILE)(ds.read(query)) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
  }

  it should "block until matching data is present, if none is found at a given time" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val query = """name("Lorenzo")""".toPrologGoal

    val readRequest = ds.read(query)
    readRequest.isCompleted should be (false)
    a [TimeoutException] should be thrownBy Await.result(readRequest, FOR_A_LONG_WHILE)
    readRequest.isCompleted should be (false)

    val dat = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_WHILE)(readRequest) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
  }

  "The Take operation" should "return and remove a Datum matching the given template from the data space" in {
    val ds = PrologAsyncDataSpace()

    val dat = """name("Lorenzo")""".toPrologTerm
    val dat2 = """name("Giovanni")""".toPrologTerm

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
    await(FOR_A_LONG_WHILE)(ds.put(dat2)) should be (dat2)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (2)

    val query = """name("Lorenzo")""".toPrologGoal
    val query2 = """name("Giovanni")""".toPrologGoal
    await(FOR_A_LONG_WHILE)(ds.take(query)) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
    await(FOR_A_LONG_WHILE)(ds.take(query2)) should be (dat2)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)
  }

  it should "block until matching data is present, if none is found at a given time" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val query = """name("Lorenzo")""".toPrologGoal
    val takeRequest = ds.take(query)
    takeRequest.isCompleted should be (false)
    a [TimeoutException] should be thrownBy Await.result(takeRequest, FOR_A_LONG_WHILE)
    takeRequest.isCompleted should be (false)

    val dat = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_WHILE)(takeRequest) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)
  }

  "The takeIfPresent operation" should "return and remove a matching datum if it is contained in the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val dat = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

    val query = """name("Lorenzo")""".toPrologGoal
    val result = await(FOR_A_LONG_WHILE)(ds.takeIfPresent(query))
    result should not be (None)
    result.get should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)
  }

  it should "remove nothing if no matching datum is found in the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val query = """name("Lorenzo")""".toPrologGoal
    var result = await(ds.takeIfPresent(query))
    result should be (None)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val dat = """name("Giovanni")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
    result = await(FOR_A_LONG_WHILE)(ds.takeIfPresent(query))
    result should be (None)
    result.isDefined should be (false)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
  }

  "The readIfPresent operation" should "return a matching datum if it is contained in the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val dat = """name("Giovanni")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

    val query = """name("Giovanni")""".toPrologGoal
    val result = await(FOR_A_LONG_WHILE)(ds.readIfPresent(query))
    result should not be (None)
    result.get should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
  }

  it should "return None if no matching datum is found in the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val query = """name("Lorenzo")""".toPrologGoal
    val result = await(FOR_A_LONG_WHILE)(ds.readIfPresent(query))
    result should be (None)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)
  }

  "The readAny operation" should "return a datum matching any of the given templates from the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val dat = """name("Giovanni")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

    await(FOR_A_LONG_WHILE)(ds.readAny(List(
      """name("Lorenzo")""".toPrologGoal,
      """name("Giovanni")""".toPrologGoal
    ))) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
  }

  it should "block until some data matching any of the templates is inserted in the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val readRequest = ds.readAny(List(
      """name("Lorenzo")""".toPrologGoal,
      """name("Giovanni")""".toPrologGoal
    ))
    readRequest.isCompleted should be (false)
    a [TimeoutException] should be thrownBy Await.result(readRequest, Duration(3, duration.SECONDS))
    readRequest.isCompleted should be (false)

    val dat = """name("Giovanni")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_LONG_WHILE)(readRequest) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

    val ds2 = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds2.size()) should be (0)

    val readRequest2 = ds2.readAny(List(
      """name("Lorenzo")""".toPrologGoal,
      """name("Giovanni")""".toPrologGoal
    ))
    readRequest2.isCompleted should be (false)
    a [TimeoutException] should be thrownBy Await.result(readRequest2, Duration(3, duration.SECONDS))
    readRequest2.isCompleted should be (false)

    val dat2 = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds2.put(dat2)) should be (dat2)
    await(FOR_A_LONG_WHILE)(readRequest2) should be (dat2)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
  }

  "The takeAny operation" should "return and remove a datum matching any of the given templates from the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val dat = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

    await(FOR_A_LONG_WHILE)(ds.takeAny(List(
      """name("Lorenzo")""".toPrologGoal,
      """name("Giovanni")""".toPrologGoal
    ))) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)
  }

  it should "block until some data matching any of the templates is inserted in the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val takeRequest = ds.takeAny(List(
      """name("Lorenzo")""".toPrologGoal,
      """name("Giovanni")""".toPrologGoal
    ))
    takeRequest.isCompleted should be (false)
    a [TimeoutException] should be thrownBy Await.result(takeRequest, Duration(3, duration.SECONDS))
    takeRequest.isCompleted should be (false)

    val dat = """name("Giovanni")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be (dat)
    await(FOR_A_LONG_WHILE)(takeRequest) should be (dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val ds2 = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds2.size()) should be (0)

    val takeRequest2 = ds2.takeAny(List(
      """name("Lorenzo")""".toPrologGoal,
      """name("Giovanni")""".toPrologGoal
    ))
    takeRequest2.isCompleted should be (false)
    a [TimeoutException] should be thrownBy Await.result(takeRequest2, Duration(3, duration.SECONDS))
    takeRequest2.isCompleted should be (false)

    val dat2 = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds2.put(dat2)) should be (dat2)
    await(FOR_A_LONG_WHILE)(takeRequest2) should be (dat2)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)
  }

  "The readEvery operation" should "return a datum matching all of the given templates from the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)

    val dat = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be(dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)

    await(FOR_A_LONG_WHILE)(ds.readEvery(List(
      """name("Lorenzo")""".toPrologGoal,
      """name(N)""".toPrologGoal
    ))) should be(dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)
  }

  it should "block until some data matching all of the templates is inserted in the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)

    val dat = """name("Lorenz")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be(dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)

    val readRequest = ds.readEvery(List(
      """name("Lorenzo")""".toPrologGoal,
      """name(N)""".toPrologGoal
    ))
    readRequest.isCompleted should be(false)
    a[TimeoutException] should be thrownBy Await.result(readRequest, Duration(3, duration.SECONDS))
    readRequest.isCompleted should be(false)

    val matchingDat = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(matchingDat)) should be(matchingDat)
    await(FOR_A_LONG_WHILE)(readRequest) should be(matchingDat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be(2)
  }

  "The takeEvery operation" should "return and remove a datum matching all of the given templates from the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)

    val dat = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be(dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)

    await(FOR_A_LONG_WHILE)(ds.takeEvery(List(
      """name("Lorenzo")""".toPrologGoal,
      """name(N)""".toPrologGoal
    ))) should be(dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)
  }

  it should "block until some data matching all of the templates is inserted in the data space" in {
    val ds = PrologAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)

    val dat = """name("Lorenz")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(dat)) should be(dat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)

    val takeRequest = ds.takeEvery(List(
      """name("Lorenzo")""".toPrologGoal,
      """name(N)""".toPrologGoal
    ))
    takeRequest.isCompleted should be(false)
    a[TimeoutException] should be thrownBy Await.result(takeRequest, Duration(3, duration.SECONDS))
    takeRequest.isCompleted should be(false)

    val matchingDat = """name("Lorenzo")""".toPrologTerm
    await(FOR_A_LONG_WHILE)(ds.put(matchingDat)) should be(matchingDat)
    await(FOR_A_LONG_WHILE)(takeRequest) should be(matchingDat)
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)
  }

  val FOR_A_WHILE: Duration = Duration(1, duration.SECONDS)
  val FOR_A_LONG_WHILE: Duration = Duration(3, duration.SECONDS)

}
