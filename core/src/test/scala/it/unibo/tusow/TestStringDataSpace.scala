/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow

import org.scalatest.{FlatSpec, Matchers}

import it.unibo.utils.concurrency.AwaitUtils._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future, TimeoutException, duration}

class TestStringDataSpace extends FlatSpec with Matchers {
  
  "DataSpace creation" should "succeed" in {
    val ds = StringAsyncDataSpace()
  }

  "The Put operation" should "insert a new Datum in the data space" in  {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)
    await(FOR_A_LONG_WHILE)(ds.put("Prova")) should be ("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)
  }

  it should "may unlock some suspended READ requests matching its argument" in {

    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val requestList = List(ds.read(".*?rov.*".r), ds.read(".*?rov.*".r), ds.read(".*?anc.*".r))
    val nPendingRequests = (list: List[Future[String]]) => list map (_.isCompleted) count(!_)

    nPendingRequests(requestList) should be (3)
    requestList foreach {
      a [TimeoutException] should be thrownBy await(FOR_A_LONG_WHILE)(_)
    }
    nPendingRequests(requestList) should be (3)

    await(FOR_A_LONG_WHILE)(ds.put("Prova")) should be ("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
    await(FOR_A_LONG_WHILE)(Future.firstCompletedOf(requestList)) should be ("Prova")

    nPendingRequests(requestList) should be (1)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

  }

  it should "satisfy at most one matching suspended take request" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val requestList = List(ds.take(".*?rov.*".r), ds.take(".*?rov.*".r), ds.take(".*?rov.*".r), ds.read(".*?rov.*".r), ds.read(".*?rov.*".r))
    val nPendingRequests = (list: List[Future[String]]) => list map (_.isCompleted) count(!_)

    nPendingRequests(requestList) should be (5)
    requestList foreach {
      a [TimeoutException] should be thrownBy await(FOR_A_LONG_WHILE)(_)
    }
    nPendingRequests(requestList) should be (5)

    await(FOR_A_LONG_WHILE)(ds.put("Prova")) should be ("Prova")
    await(FOR_A_LONG_WHILE)(Future.firstCompletedOf(requestList)) should be ("Prova")
    nPendingRequests(requestList) should be >= 2
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    await(FOR_A_LONG_WHILE)(ds.put("Prova")) should be ("Prova")
    await(FOR_A_LONG_WHILE)(Future.firstCompletedOf(requestList)) should be ("Prova")
    nPendingRequests(requestList) should be >= 1
    await(ds.size()) should be (0)

    await(FOR_A_LONG_WHILE)(ds.put("Prova")) should be ("Prova")
    await(FOR_A_LONG_WHILE)(Future.firstCompletedOf(requestList)) should be ("Prova")
    nPendingRequests(requestList) should be >= 0
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    await(FOR_A_LONG_WHILE)(ds.put("Prova")) should be ("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

  }

  "The Read operation" should "return a Datum matching the given template from the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)
    await(FOR_A_LONG_WHILE)(ds.put("Prova")) should be ("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)

    await(FOR_A_LONG_WHILE)(ds.read(".*?rov.*".r)) should be("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)
  }

  it should "block until matching data is present, if none is found at a given time" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val readRequest = ds.read(".*?rov.*".r)
    readRequest.isCompleted should be (false)
    a [TimeoutException] should be thrownBy Await.result(readRequest, Duration(3, duration.SECONDS))
    readRequest.isCompleted should be (false)

    await(FOR_A_LONG_WHILE)(ds.put("Prova")) should be ("Prova")
    await(FOR_A_LONG_WHILE)(readRequest) should be ("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

  }

  "The Take operation" should "return and remove a Datum matching the given template from the data space" in {
    val ds = StringAsyncDataSpace()
    val A_SHORT_WHILE = Duration(5, duration.SECONDS)

    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)
    await(FOR_A_LONG_WHILE)(ds.put("Prova")) should be("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)
    await(FOR_A_LONG_WHILE)(ds.put("Ancona")) should be("Ancona")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(2)

    await(FOR_A_LONG_WHILE)(ds.take(".*?rov.*".r)) should be("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)

    await(FOR_A_LONG_WHILE)(ds.take(".*?con.*".r)) should be("Ancona")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)
  }

  it should "block until matching data is present, if none is found at a given time" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val takeRequest = ds.take(".*?rov.*".r)
    takeRequest.isCompleted should be (false)
    a [TimeoutException] should be thrownBy Await.result(takeRequest, Duration(3, duration.SECONDS))
    takeRequest.isCompleted should be (false)

    await(FOR_A_LONG_WHILE)(ds.put("Prova")) should be ("Prova")
    await(FOR_A_LONG_WHILE)(takeRequest) should be ("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

  }

  "The takeIfPresent operation" should "return and remove a matching datum if it is contained in the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    await(FOR_A_LONG_WHILE)(ds.put("Prova")) should be ("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
    val result = await(FOR_A_LONG_WHILE)(ds.takeIfPresent(".*?rov.*".r))
    result should not be None
    result.get should be ("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)
  }

  it should "remove nothing if no matching datum is found in the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    var result = await(FOR_A_LONG_WHILE)(ds.takeIfPresent(".*?rov.*".r))
    result should be (None)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    await(FOR_A_LONG_WHILE)(ds.put("Ancona")) should be ("Ancona")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
    result = await(FOR_A_LONG_WHILE)(ds.takeIfPresent(".*?rov.*".r))
    result should be (None)
    result.isDefined should be (false)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

  }

  "The readIfPresent operation" should "return a matching datum if it is contained in the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    await(FOR_A_LONG_WHILE)(ds.put("Prova")) should be ("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
    var result = await(FOR_A_LONG_WHILE)(ds.readIfPresent(".*?rov.*".r))
    result should not be None
    result.get should be ("Prova")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

  }

  it should "return None if no matching datum is found in the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val result = await(FOR_A_LONG_WHILE)(ds.readIfPresent(".*?anc.*".r))
    result should be (None)
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)
  }

  "The readAny operation" should "return a datum matching any of the given templates from the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    await(FOR_A_LONG_WHILE)(ds.put("Lorenzo")) should be ("Lorenzo")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

    await(FOR_A_LONG_WHILE)(ds.readAny(List(".*?van.*".r, ".*?ore.*".r, ".*?pol.*".r))) should be ("Lorenzo")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
  }

  it should "block until some data matching any of the templates is inserted in the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val readRequest = ds.readAny(List(".*?rov.*".r, ".*?ore.*".r, ".*?van.*".r))
    readRequest.isCompleted should be (false)
    a [TimeoutException] should be thrownBy Await.result(readRequest, Duration(3, duration.SECONDS))
    readRequest.isCompleted should be (false)

    await(FOR_A_LONG_WHILE)(ds.put("Giovanni")) should be ("Giovanni")
    await(FOR_A_LONG_WHILE)(readRequest) should be ("Giovanni")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

    val ds2 = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds2.size()) should be (0)

    val readRequest2 = ds2.readAny(List(".*?rov.*".r, ".*?ore.*".r, ".*?van.*".r))
    readRequest2.isCompleted should be (false)
    a [TimeoutException] should be thrownBy Await.result(readRequest2, Duration(3, duration.SECONDS))
    readRequest2.isCompleted should be (false)

    await(FOR_A_LONG_WHILE)(ds2.put("Lorenzo")) should be ("Lorenzo")
    await(FOR_A_LONG_WHILE)(readRequest2) should be ("Lorenzo")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)
  }

  "The takeAny operation" should "return and remove a datum matching any of the given templates from the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    await(FOR_A_LONG_WHILE)(ds.put("Lorenzo")) should be ("Lorenzo")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (1)

    await(FOR_A_LONG_WHILE)(ds.takeAny(List(".*?van.*".r, ".*?ore.*".r, ".*?pol.*".r))) should be ("Lorenzo")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)
  }

  it should "block until some data matching any of the templates is inserted in the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val takeRequest = ds.takeAny(List(".*?rov.*".r, ".*?ore.*".r, ".*?van.*".r))
    takeRequest.isCompleted should be (false)
    a [TimeoutException] should be thrownBy Await.result(takeRequest, Duration(3, duration.SECONDS))
    takeRequest.isCompleted should be (false)

    await(FOR_A_LONG_WHILE)(ds.put("Giovanni")) should be ("Giovanni")
    await(FOR_A_LONG_WHILE)(takeRequest) should be ("Giovanni")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)

    val takeRequest2 = ds.takeAny(List(".*?rov.*".r, ".*?ore.*".r, ".*?van.*".r))
    takeRequest2.isCompleted should be (false)
    a [TimeoutException] should be thrownBy Await.result(takeRequest2, Duration(3, duration.SECONDS))
    takeRequest2.isCompleted should be (false)

    await(FOR_A_LONG_WHILE)(ds.put("Lorenzo")) should be ("Lorenzo")
    await(FOR_A_LONG_WHILE)(takeRequest2) should be ("Lorenzo")
    await(FOR_A_LONG_WHILE)(ds.size()) should be (0)
  }

  "The readEvery operation" should "return a datum matching all of the given templates from the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)

    await(FOR_A_LONG_WHILE)(ds.put("Lorenzo")) should be("Lorenzo")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)

    await(FOR_A_LONG_WHILE)(ds.readEvery(List(".*?ore.*".r, ".*?nzo.*".r, ".*?Lor.*".r))) should be("Lorenzo")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)
  }

  it should "block until some data matching all of the templates is inserted in the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)

    await(FOR_A_LONG_WHILE)(ds.put("Lorenz")) should be("Lorenz")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)

    val readRequest = ds.readEvery(List(".*?ore.*".r, ".*?nzo.*".r, ".*?Lor.*".r))
    readRequest.isCompleted should be(false)
    a[TimeoutException] should be thrownBy Await.result(readRequest, Duration(3, duration.SECONDS))
    readRequest.isCompleted should be(false)

    await(FOR_A_LONG_WHILE)(ds.put("Lorenzo")) should be("Lorenzo")
    await(FOR_A_LONG_WHILE)(readRequest) should be("Lorenzo")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(2)
  }

  "The takeEvery operation" should "return and remove a datum matching all of the given templates from the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)

    await(FOR_A_LONG_WHILE)(ds.put("Lorenzo")) should be("Lorenzo")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)

    await(FOR_A_LONG_WHILE)(ds.takeEvery(List(".*?ore.*".r, ".*?nzo.*".r, ".*?Lor.*".r))) should be("Lorenzo")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)
  }

  it should "block until some data matching all of the templates is inserted in the data space" in {
    val ds = StringAsyncDataSpace()

    await(FOR_A_LONG_WHILE)(ds.size()) should be(0)

    await(FOR_A_LONG_WHILE)(ds.put("Lorenz")) should be("Lorenz")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)

    val takeRequest = ds.takeEvery(List(".*?ore.*".r, ".*?nzo.*".r, ".*?Lor.*".r))
    takeRequest.isCompleted should be(false)
    a[TimeoutException] should be thrownBy Await.result(takeRequest, Duration(3, duration.SECONDS))
    takeRequest.isCompleted should be(false)

    await(FOR_A_LONG_WHILE)(ds.put("Lorenzo")) should be("Lorenzo")
    await(FOR_A_LONG_WHILE)(takeRequest) should be("Lorenzo")
    await(FOR_A_LONG_WHILE)(ds.size()) should be(1)
  }

  val FOR_A_WHILE: Duration = Duration(1, duration.SECONDS)

  val FOR_A_LONG_WHILE: Duration = Duration(3, duration.SECONDS)

}

