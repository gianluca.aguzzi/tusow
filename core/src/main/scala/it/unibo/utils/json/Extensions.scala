/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.utils.json

import java.io.IOException

import com.fasterxml.jackson.databind.{JsonNode, ObjectMapper}
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import com.jayway.jsonpath.{InvalidPathException, JsonPath}

object Extensions {

  val yamlMapper = new YAMLMapper()

  implicit class StringToJSONObjExtension(string: String) {
    def toJsonNode: JsonNode = {
      if (string.isEmpty) {
        throw new IOException()
      }
      yamlMapper.readTree(string)
    }
  }

  implicit class StringToJSONPathExtension(string: String) {
    @throws(classOf[InvalidPathException])
    def toJsonPath: JsonPath = {
     JsonPath.compile(string)
    }
  }
}
