/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ArrayNode
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider
import com.jayway.jsonpath.{Configuration, JsonPath, Option}

private class JsonAsyncDataSpace extends AbstractAsyncDataSpace[JsonNode, JsonPath] {
  override protected def datumMatchesTemplate(datum: JsonNode, template: JsonPath): Boolean = {
    val queryResult: ArrayNode = template.read(datum, JsonAsyncDataSpace.CONFIGURATION)
    queryResult.size() > 0
  }
}

object JsonAsyncDataSpace {
  private val CONFIGURATION: Configuration = Configuration.builder()
    .options(Option.ALWAYS_RETURN_LIST)
    .options(Option.SUPPRESS_EXCEPTIONS)
    .jsonProvider(new JacksonJsonNodeJsonProvider())
    .build()

  def apply(): AsyncDataSpace[JsonNode, JsonPath] = new JsonAsyncDataSpace()
}