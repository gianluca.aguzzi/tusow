/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow

trait SyncDataSpace[Datum, Template] {

  def put(datum: Datum): Unit

  def take(template: Template): Datum

  def read(template: Template): Datum

  def takeIfPresent(template: Template): Option[Datum]

  def readIfPresent(template: Template): Option[Datum]

  def size(): Int
}

object SyncDataSpace {

  object Extensions {

    implicit class SyncToAsyncExtension[D, T](ads: AsyncDataSpace[D, T]) {
      lazy val sds: SyncDataSpace[D, T] = SyncDataSpace[D, T](ads)

      def asSync(): SyncDataSpace[D, T] = sds
    }

  }

  def apply[D, T](ads: AsyncDataSpace[D, T]): SyncDataSpace[D, T] = new AsyncToSyncDecorator[D, T](ads)
}