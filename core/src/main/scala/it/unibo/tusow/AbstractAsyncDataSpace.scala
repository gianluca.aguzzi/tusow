/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow

import scala.collection.mutable.ListBuffer
import scala.concurrent.{Future, Promise}
import scala.util.Random


protected abstract class AbstractAsyncDataSpace[Datum, Template] extends AsyncDataSpace[Datum, Template] {

  import scala.concurrent.ExecutionContext.Implicits.global

  /**
    * Describes the structure of a pending request. It is composed as a tuple containing
    * a template, a promise through which the request can be completed, and a type describing
    * the nature of the request.
    */
  private case class Request(templates: List[Template], promise: Promise[Datum], reqType: RequestType, logic: MatchingLogic)

  /**
    * A collection of this DataSpace's data.
    */
  private var dataSpace: ListBuffer[Datum] = ListBuffer.empty
  /**
    * A collection of this DataSpace's pending requests.
    */
  private var incomingRequests: ListBuffer[Request] = ListBuffer.empty

  private val random: Random = new Random()

  // TODO fix scaladoc
  protected def datumMatchesTemplate(datum: Datum, template: Template): Boolean

  override def take(template: Template): Future[Datum] = {
    val promise = Promise[Datum]

    Future {
      var selected: Option[Datum] = None

      this.synchronized {
        selected = selectOneDatumMatching(template)
        if (selected.isDefined) {
          val selectedDatum: Datum = selected.get
          dataSpace -= selectedDatum
          promise.trySuccess(selectedDatum)
        } else {
          incomingRequests += Request(List(template), promise, RequestType.Take, MatchingLogic.Any)
        }
      }
    }

    promise.future
  }

  private def selectOneDatumMatching(template: Template): Option[Datum] = {
    selectOneRandomly(dataSpace.toStream.filter(datumMatchesTemplate(_, template)))
  }

  private def servePendingRequestsMatchingDatum(datum: Datum): Unit = {
    val matchingRequests = Random.shuffle(incomingRequests.filter(r =>
      r.logic match {
        case MatchingLogic.Any => datumMatchesAnyTemplate(datum, r.templates)
        case MatchingLogic.Every => datumMatchesAllTemplates(datum, r.templates)
      }
    ))

    matchingRequests.takeWhile(_.reqType == RequestType.Read).foreach {
      case r@Request(_, p, _, _) =>
        p.trySuccess(datum)
        incomingRequests -= r
    }

    matchingRequests.find(_.reqType == RequestType.Take).foreach {
      case r@Request(_, p, _, _) =>
        p.trySuccess(datum)
        incomingRequests -= r
        dataSpace -= datum
    }

  }

  private def datumMatchesAnyTemplate(datum: Datum, templates: List[Template]): Boolean = {
    templates.exists(t => datumMatchesTemplate(datum, t))
  }

  private def datumMatchesAllTemplates(datum: Datum, templates: List[Template]): Boolean = {
    templates.forall(t => datumMatchesTemplate(datum, t))
  }

  private def servePendingRequests(): Unit = {
    dataSpace foreach servePendingRequestsMatchingDatum
  }

  /**
    * Inserts a new [[Datum]] in this data space.
    * <br>
    * This implementation tries to unlock as many suspended requests as possible, until a
    * <code>take</code> request is satisfied. This is done in a random fashion: therefore, one
    * should not make assumptions on the number of unlocked requests, nor on the order in which
    * those requests are satisfied.
    *
    * @param datum the new datum to be inserted in the data space
    * @return a [[scala.concurrent.Future]] containing the inserted Datum
    */
  override def put(datum: Datum): Future[Datum] = {
    Future {
      this.synchronized {
        dataSpace += datum
        servePendingRequestsMatchingDatum(datum)
      }
      datum
    }
  }

  private def selectOneRandomly[T](seq: Seq[T]): Option[T] = {
    if (seq.isEmpty) None
    else Some(seq.drop(random.nextInt(seq.length)).head)
  }

  override def read(template: Template): Future[Datum] = {
    val promise = Promise[Datum]

    Future {
      var selected: Option[Datum] = None
      this.synchronized {
        selected = selectOneDatumMatching(template)
        if (selected.isDefined) {
          promise.trySuccess(selected.get)
        } else {
          incomingRequests += Request(List(template), promise, RequestType.Read, MatchingLogic.Any)
        }
      }
    }

    promise.future
  }

  override def get(): Future[List[Datum]] = Future {
    this.synchronized {
      dataSpace.clone().toList
    }
  }


  override def clear(): Future[List[Datum]] = Future {
    this.synchronized {
      val temp = dataSpace.clone().toList
      dataSpace.clear()
      temp
    }
  }

  override def putAll(data: Seq[Datum]): Future[Seq[Datum]] = Future {
    this.synchronized {
      dataSpace ++= data
      servePendingRequests()
    }
    data
  }

  override def set(data: Seq[Datum]): Future[Seq[Datum]] = Future {
    this.synchronized {
      dataSpace.clear()
      dataSpace ++= data
      servePendingRequests()
    }
    data
  }

  override def takeAll(template: Template): Future[List[Datum]] = Future {
    this.synchronized {
      val temp = dataSpace.filter(datumMatchesTemplate(_, template)).toList
      dataSpace --= temp
      temp
    }
  }

  override def readAll(template: Template): Future[List[Datum]] = Future {
    this.synchronized {
      dataSpace.filter(datumMatchesTemplate(_, template)).toList
    }
  }

  override def readAny(templates: List[Template]): Future[Datum] = {
    val promise = Promise[Datum]

    Future {
      this.synchronized {
        val selected = templates.map(selectOneDatumMatching(_)).collect { case Some(d) => d }
        if (selected.nonEmpty) {
          promise.trySuccess(selected.head)
        } else {
          incomingRequests += Request(templates, promise, RequestType.Read, MatchingLogic.Any)
        }
      }
    }

    promise.future
  }

  override def takeAny(templates: List[Template]): Future[Datum] = {
    val promise = Promise[Datum]

    Future {
      this.synchronized {
        val selected = templates.map(selectOneDatumMatching(_)).collect { case Some(d) => d }
        if (selected.nonEmpty) {
          val firstSelected = selected.head
          dataSpace -= firstSelected
          promise.trySuccess(firstSelected)
        } else {
          incomingRequests += Request(templates, promise, RequestType.Take, MatchingLogic.Any)
        }
      }
    }

    promise.future
  }

  override def readEvery(templates: List[Template]): Future[Datum] = {
    val promise = Promise[Datum]

    Future {
      this.synchronized {
        val selected = templates.map(selectOneDatumMatching(_))
        if (selected.forall(_.isDefined)) {
          promise.trySuccess(selected.head.get)
        } else {
          incomingRequests += Request(templates, promise, RequestType.Read, MatchingLogic.Every)
        }
      }
    }

    promise.future
  }

  override def takeEvery(templates: List[Template]): Future[Datum] = {
    val promise = Promise[Datum]

    Future {
      this.synchronized {
        val selected = templates.map(selectOneDatumMatching(_))
        if (selected.forall(_.isDefined)) {
          val firstSelected = selected.head.get
          dataSpace -= firstSelected
          promise.trySuccess(firstSelected)
        } else {
          incomingRequests += Request(templates, promise, RequestType.Take, MatchingLogic.Every)
        }
      }
    }

    promise.future
  }

  override def size(): Future[Int] = Future {
    this.synchronized {
      dataSpace.size
    }
  }

  override def takeIfPresent(template: Template): Future[Option[Datum]] = Future {
    var selected: Option[Datum] = None

    this.synchronized {
      selected = selectOneDatumMatching(template)
      if (selected.isDefined) {
        dataSpace -= selected.get
      }
    }

    selected
  }


  override def readIfPresent(template: Template): Future[Option[Datum]] = Future {
    var selected: Option[Datum] = None
    this.synchronized {
      selected = selectOneDatumMatching(template)
    }
    selected
  }

  override def toString: String =
    f"${this.getClass.getSimpleName}(${this.dataSpace.mkString(", ")})"
}
