/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow

import scala.concurrent.Future

/**
  * Provides a general definition of a Data Space.
  * <br>
  * A Data Space is a collection of data shared between agents. Agents may communicate via the data space
  * using a simple set of operations made to add, read or remove data from the space.
  * <br>
  * Agents may search for data using templates: a template allows to only select the desired data
  * from the data space.
  *
  * @tparam Datum The type of the elements contained in this data space.
  * @tparam Template The type of the query used to find elements in this data space.
  */
trait AsyncDataSpace[Datum, Template] {

  /**
    * Inserts a new [[Datum]] in this data space.
    * <br>
    * This operation unlocks a subset of the suspended requests whose templates match the newly
    * inserted Datum. One should not make assumptions on the number of unlocked requests, nor on the
    * order in which those requests are satisfied.
    *
    * @param datum the new datum to be inserted in the data space
    * @return a [[scala.concurrent.Future]] containing the inserted Datum
    */
  def put(datum: Datum): Future[Datum]

  def putAll(data: Seq[Datum]): Future[Seq[Datum]]

  def set(data: Seq[Datum]): Future[Seq[Datum]]

  /**
    * Returns a [[Datum]] matching the given template, and removes it from this data space.
    * <br>
    * If no matching datum is found, the operation suspends until the requested datum is available.

    * @param template the template describing the requested Datum
    * @return a [[scala.concurrent.Future]] containing the requested Datum
    */
  def take(template: Template): Future[Datum]

  def takeAll(template: Template): Future[List[Datum]]

  /**
    * Returns a [[Datum]] matching the given template from this data space, leaving this data
    * space's content unchanged.
    * <br>
    * If no matching datum is found, the operation suspends until the requested datum is available.
    *
    * @param template the template describing the requested Datum
    * @return a [[scala.concurrent.Future]] containing the requested Datum
    */
  def read(template: Template): Future[Datum]

  def get(): Future[List[Datum]]

  def clear(): Future[List[Datum]]

  def readAll(template: Template): Future[List[Datum]]

  /**
    * Returns a [[Datum]] matching the given template, and removes it from this data space.
    * <br>
    * If no matching datum is found, the operation terminates with no result.
    *
    * @param template the template describing the requested Datum
    * @return a future Option[Datum] containing the requested datum. If no datum is found, a Future containing None
    *         is returned.
    */
  def takeIfPresent(template: Template): Future[Option[Datum]]

  /**
    * Returns a [[Datum]] matching the given template, leaving this data space's content unchanged.
    * <br>
    * If no matching datum is found, the operation terminates with no result.

    * @param template the template describing the requested Datum
    * @return a future Option[Datum] containing the requested datum. If no datum is found, a Future containing None
    *         is returned.
    */
  def readIfPresent(template: Template): Future[Option[Datum]]

  def readAny(templates: List[Template]): Future[Datum]

  def takeAny(templates: List[Template]): Future[Datum]

  def readEvery(templates: List[Template]): Future[Datum]

  def takeEvery(templates: List[Template]): Future[Datum]

  /**
    * Returns the number of [[Datum]] elements present in this data space.
    * @return this data space's size
    */
  def size(): Future[Int]
}