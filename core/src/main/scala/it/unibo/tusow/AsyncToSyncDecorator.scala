/*
 * Copyright (c) 2018 Giovanni Ciatto and others listed into
 * the NOTICE file in the root of this distribution.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.unibo.tusow

import scala.concurrent.Await

private[tusow] class AsyncToSyncDecorator[D, T](private val decorated: AsyncDataSpace[D, T]) extends SyncDataSpace[D, T] {
  import it.unibo.utils.concurrency.AwaitUtils._

  override def put(datum: D): Unit = await {
    decorated.put(datum)
  }

  override def take(template: T): D = await {
    decorated.take(template)
  }

  override def read(template: T): D = await {
    decorated.read(template)
  }

  override def takeIfPresent(template: T): Option[D] = await {
    decorated.takeIfPresent(template)
  }

  override def readIfPresent(template: T): Option[D] = await {
    decorated.readIfPresent(template)
  }

  override def size(): Int = await {
    decorated.size()
  }


  override def toString = s"AsyncToSyncDecorator($decorated)"
}
