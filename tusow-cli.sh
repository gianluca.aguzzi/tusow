#!/bin/bash

usage() {
    echo "usage: tusow-cli (read | put | take) [-u url | [-h host] [--json | --str] [-n name]] [-d content | -f file] [-p | -s] [-v]"
    echo "  -u url          specify full URL where to send the request"
    echo "  -h host         specify the host name where to send the request"
    echo "  -n name         specify the dataspace name where to send the request"
    echo "  -json           specify that the request should be sent to a JSON dataspace"
    echo "  -string         specify that the request should be sent to a textual dataspace"
    echo "  -d content      specify either the requested data pattern in a 'read' or 'take' request"
    echo "                      or the datum to be inserted through a 'put' request"
    echo "  -f file         specify data to be sent through a 'put' request"
    echo "  -p              flags the 'read' or 'take' request as predicative"
    echo "  -s              flags the 'put' request as synchronous"
    echo "  -v --verbose    display verbose output"
    echo "  -h --help       display this help message"
    exit
}

#OPTIND=3
OPTIND=2

hostopt="http://localhost"
portopt="8080"
dstypeopt="string"
dsnameopt="default"

pred="predicative=false"

fileflag=0

accept="text/plain"
content_type_template="text/plain"
content_type_datum="text/plain"

while getopts ":vf:t:psu:h:n:-:d:" name
do
    case $name in

        u)
            urlopt="$OPTARG"
            ;;
        h)
            hostopt="$OPTARG"
            ;;
        n)
            dsnameopt="$OPTARG"
            ;;
        v)
            vopt="-v"
            ;;
        f)
            fileopt=`cat $OPTARG`
            fileflag=1
            ;;
        d)
            contentopt="$OPTARG"
            ;;
        p)
            pred="predicative=true"
            ;;
        s)
            sync="sync=true"
            ;;
        -)
            case "$OPTARG" in
                help)
                    usage >&2
                    exit
                    ;;
                verbose)
                    vopt="-v"
                    ;;
                json)
                    dstypeopt="json"
                    accept="application/json"
                    content_type_template="text/plain"
                    content_type_datum="application/json"
                    ;;
                str)
                    dstypeopt="string"
                    accept="text/plain"
                    content_type_template="text/plain"
                    content_type_datum="text/plain"
                    ;;
                *)
                    echo "tusow: unknown option $OPTARG (type tusow --help to display usage info)"
                    exit
                    ;;
            esac
            ;;
        *)
            echo "tusow: unknown option $OPTARG (type tusow --help to display usage info)"
            usage
            ;;
    esac
done

# shift $(($OPTIND -1))
#echo $#
if ([ -z "$contentopt" ] && [ $fileflag == 0 ]) || ( ! [ -z "$contentopt" ] && ! [ $fileflag == 0 ] ); then
    usage
fi

case $1 in
    read)
        methodopt=GET
        ;;
    put)
        methodopt=POST
        ;;
    take)
        methodopt=DELETE
        ;;
    *)
        usage
        ;;
esac

if [ -z "$urlopt" ]; then
    host="$hostopt:$portopt/data_spaces/$dstypeopt/$dsnameopt"
else
    host="$urlopt"
fi

case "$methodopt" in
    GET | DELETE)
        # cmd="curl -X $methodopt -G $host --data-urlencode template=$contentopt --data-urlencode $pred --silent $vopt"
        # echo "    $cmd"
        # out=$($cmd)

        out=$(curl -X $methodopt -G $host -H "Accept: $accept" -H "Content-Type: $content_type_template" --data-urlencode template="$contentopt" --data-urlencode $pred --silent $vopt)

        ;;
    POST)
        # cmd="curl -X $methodopt -G $host -H \"Content-Type: text/plain\" --data '$datumopt' --silent $vopt"
        # echo "    $cmd"
        # out=$($cmd)

        out=$(curl -X $methodopt "$host" -H "Accept: $accept" -H "Content-Type: $content_type_datum" -d "$contentopt" --silent $vopt)
        ;;
    *)
        echo "error"
        exit
        ;;
esac

if test "$?" != "0"; then
    echo "tusow: error while connecting to dataspace"
    exit 1
fi

echo "$out"